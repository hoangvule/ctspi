# README #
This repo contains the application for Calumino CTS


### Installation Instruction ###
Login to the Rapberry Pi console (via SSH), enter the following commands one by one:

`git clone https://bitbucket.org/hoangvule/ctspi.git`   
`cd ctspi`  
`sudo chmod +x install.sh`  
`./install.sh`  

The application should run automatically if the installation is successful
