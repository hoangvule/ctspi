#!/bin/bash
#-------------------------------------------------
#  Calumino CTS Installation Script
#  Version 1.0.20210715.1
#-------------------------------------------------

# Clean up first
echo "Clean up workspace..."
mkdir -p /home/pi/calumino/app
rm -rf /home/pi/calumino/app/*

# Download neccessary packages
# 1. Supervisor
echo "Installing supervisor..."
sudo apt-get update
sudo apt-get install supervisor
sudo cp scripts/ctspi.conf /etc/supervisor/conf.d/ctspi.conf
sudo mkdir -p /var/log/supervisor

# 2.SocketXP
echo "Installing SocketXP..."
curl -O https://portal.socketxp.com/download/iot/socketxp_install.sh && chmod +wx socketxp_install.sh && sudo ./socketxp_install.sh -a "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Mjc3ODUwOTksImlkIjoiaG9hbmdAY2FsdW1pbm8uY29tIn0.OdaqBsQSN8iUcthQZ3ooze5Mtc2NlE6_yRB99u9RgT0"

# 3.Copy source files to app directory
echo "Copy files..."
cp -R app/* /home/pi/calumino/app

# Trigger services
echo "Starting application..."
sudo service supervisor restart

echo "Done"