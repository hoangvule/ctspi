import json
import requests
import logging

from config import *

logger = logging.getLogger('ctspi')
logger.setLevel(logging.INFO)

def load_registration_info():
    try:
        with open(registrationInfoFile, 'r') as f:
            reginfo = json.load(f)
            if ('apiUrl' in reginfo) and ('apiKey' in reginfo):
                return reginfo
            else:
                return None
    except Exception as e:
        logger.error('Could not load registration info')
        logger.error(e)
        return None
        
def register_device(dev_serial):
    try:
        logger.info('Start registering...')
        headers = { 'Content-Type': 'application/json'}
        url = registrationUrl+'/registerDevice'
        payload = {'registration_key': registrationKey, 'serial_number': dev_serial, 'device_type': deviceType, 'timezone': timezone}
        r = requests.post(url, headers=headers, params=payload, timeout=10)
        response = {}
        response['body'] = r.text
        data = json.loads(response['body'])
        with open(registrationInfoFile, 'w+') as f:
            f.write(json.dumps(data))
        logger.info('Save registration info done!')
        return data
    except Exception as e:
        logger.error('Could not register device')
        logger.error(e)
        return None

def get_device_info(dev_serial, reginfo):
    try:
        logger.info('Retrieving info from server...')
        headers = { 'Content-Type': 'application/json'}
        url = reginfo['apiUrl'] + '/registration/configureDevice/' + dev_serial
        payload = {'device_key': reginfo['apiKey']}
        r = requests.get(url, headers=headers, params=payload, timeout=10)
        response = {}
        response['body'] = r.text
        data = json.loads(response['body'])
        logger.info('Get info from server done!')
        return data
    except Exception as e:
        logger.error('Could not get device info from server')
        logger.error(e)
        return None






