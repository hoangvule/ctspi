from __future__ import print_function

import datetime
import json
import logging
import ssl
import uuid
import paho.mqtt.client as mqtt

from paho.mqtt.client import MQTT_ERR_SUCCESS, MQTT_ERR_NO_CONN

''' MQTT handler utility
'''
rc=7
class MqttHandler(object):

    def __init__(self):
        self.log = logging.getLogger(__name__)

        self.onMessageReceived = None
        self.onPublished = None
        self.onConnect = None
        self.onDisconnect = None
        self.onSubscribe = None
        self.onUnsubscribe = None
        self.client = None
        self.isConnected = False
        self.recvCount = 0
        self.sendCount = 0
        self._activeTopics = []
        self.lastPubTime = datetime.datetime(1970, 1, 1)
        self.preventErrors = True  # catch errors in on_message or on_publish

        self.configure()

    def configure(self, host='localhost', port=1883, user='guest', passwd='guest', clientId=str(uuid.uuid4()),
                  cleanSession=True, keepAlive=10, maxInflight=0, usessl=False):
        self.mqttHost = host
        self.mqttPort = int(port)
        self.brokerUser = user
        self.brokerPass = passwd
        self.clientId = clientId
        self.mqttCleanSession = int(cleanSession)
        self.mqttKeepAlive = int(keepAlive)
        self.ssl = usessl
        self.maxInflight = maxInflight

    def on_connect(self, client, userdata, flags, rc):
        self.isConnected = False

        if rc == 0:
            self.log.debug("MQTT CONNACK returned 0: Connection successful")
            self.isConnected = True
        elif rc == 1:
            self.log.warning("MQTT CONNACK returned 1: Connection refused -incorrect protocol version")
        elif rc == 2:
            self.log.warning("MQTT CONNACK returned 2: Connection refused - invalid client identifier")
        elif rc == 3:
            self.log.warning("MQTT CONNACK returned 3: Connection refused - server unavailable")
        elif rc == 4:
            self.log.warning("MQTT CONNACK returned 4: Connection refused - bad username or password")
        elif rc == 5:
            self.log.warning("MQTT CONNACK returned 5: Connection refused - not authorised")
        else:
            self.log.warning("MQTT CONNACK returned 6-255: Currently unused")

        try:
            if self.onConnect is not None:
                self.onConnect(client, userdata, flags, rc)

        except Exception as e:
            self.log.error('error in callback onConnect: %s' % (str(e),))

    def on_disconnect(self, client, userdata, rc=0):
        self.clearTopics()

        self.isConnected = False
        self.log.debug("MQTT Disconnected result code " + str(rc))

        try:
            if self.onDisconnect is not None:
                self.onDisconnect(client, userdata, rc)

        except Exception as e:
            self.log.error('error in callback onDisconnect: %s' % (str(e),))

    def on_message(self, client, userdata, message):
        self.recvCount += 1

        try:
            if self.onMessageReceived is not None:
                self.onMessageReceived(client, userdata, message)

        except Exception as e:
            self.log.error('error in callback onMessageReceived: %s' % (str(e),))

            if not self.preventErrors:
                raise

    def on_publish(self, client, userdata, result):
        self.sendCount += 1

        try:
            if self.onPublished is not None:
                self.onPublished(client, userdata, result)

        except Exception as e:
            self.log.error('error in callback onPublished: %s' % (str(e),))

            if not self.preventErrors:
                raise

    def connect(self):
        if self.client is not None:
            self.client.loop_stop()
            self.log.warning('stopped old MQTT client')

        self.log.info('creating MQTT client')
        self.client = mqtt.Client(self.clientId, clean_session=self.mqttCleanSession)
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_message = self.on_message
        self.client.on_publish = self.on_publish
        self.client.on_subscribe = self.onSubscribe
        self.client.on_unsubscribe = self.onUnsubscribe
        self.client.username_pw_set(self.brokerUser, self.brokerPass)

        if self.ssl:
            self.client.tls_set_context(context=ssl.create_default_context(ssl.Purpose.SERVER_AUTH))

        if self.maxInflight > 0:
            self.client.max_inflight_messages_set(self.maxInflight)

        self.client.connect(self.mqttHost, self.mqttPort, self.mqttKeepAlive)
        self.client.loop_start()

    def stop(self):
        if self.client is not None:
            self.log.info('stopping client')
            self.client.loop_stop()
            self.client = None

    def stats(self):
        return {'isConnected': self.isConnected, 'sent': self.sendCount, 'received': self.recvCount,
                'topics': len(self._activeTopics)}

    def topics(self):
        return list(self._activeTopics)

    def subscribe(self, topic, qos=1):
        if self.client is None:
            self.log.error('cannot subscribe while not connected')
            return [MQTT_ERR_NO_CONN]

        if topic in self._activeTopics:
            self.log.debug('already subscribed to: %s' % (topic,))
            return [MQTT_ERR_SUCCESS]

        self.log.debug('subscribing to: %s' % (topic,))
        msg = self.client.subscribe(topic, qos=qos)
        self._activeTopics.append(topic)

        return msg

    def unsubscribe(self, topic):
        if self.client is None:
            self.log.error('cannot unsubscribe while not connected')
            return

        self.client.unsubscribe(topic)
        self._activeTopics.remove(topic)

    def secSinceLastPublish(self):
        now = datetime.datetime.now()
        return (now - self.lastPubTime).total_seconds()

    def publish(self, topic, message, qos=1):
        global rc
        if self.client is None:
            self.log.error('cannot publish while not connected')
            return

        #(rc, mid) = self.client.publish(topic, message, qos=qos)
        (rc, mid) = self.client.publish(topic, message, qos=qos)
        #print ("rc",rc)
        self.lastPubTime = datetime.datetime.now()

    def publishJson(self, topic, message, qos=1):
        return self.publish(topic, json.dumps(message), qos)

    def clearTopics(self):
        self._activeTopics.clear()
