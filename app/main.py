#!python
#cython: language_level=3
import os
import numpy as np
import signal
import time
import socket
import RPi.GPIO as GPIO
import mqtt
import shlex
import subprocess

from mqtt import MqttHandler
from io import BytesIO
import sys
from cts import *
from register import *
import logging
import logging.handlers
import psutil
import threading
import json

GPIO.setmode(GPIO.BCM)

INTERNET_CHECK_INTERVAL = 300 #sec

network_connected = 0

logger = logging.getLogger('ctspi')
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
os.system('mkdir -p /home/pi/log')
file_handler = logging.handlers.RotatingFileHandler('/home/pi/log/ctspi.log','a',maxBytes=256000, backupCount=100)
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler(sys.stdout)
console_handler.setLevel(logging.INFO)
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


class SigtermHandler:
    stop_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exitOnSignterm)
        signal.signal(signal.SIGTERM, self.exitOnSignterm)

    def exitOnSignterm(self, signo, stackframe):
        self.stop_now = True



def is_internet_on(host='8.8.8.8', port=53, timeout=3):
    """
    Poll a host to see if we have an internet connection.
    By default polls Google Public DNS on port 53 (dns).
    """
    global network_connected
    try:
        socket.setdefaulttimeout(timeout)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((host, port))
        sock.close()
        print("Internet OK")
        network_connected = 1
        return True
    except socket.error as ex:
        logger.error("Internet checker fault: %s" % ex)
        network_connected = 0
        return False


def keeper_function(name):
    timer = time.time()
    periodic_checking_timer = timer + INTERNET_CHECK_INTERVAL
    fail = 0
    
    try:
        while True:
            time.sleep(1.0)
            now = time.time()
            if(now >= periodic_checking_timer):
                if (is_internet_on()==False):
                    fail+=1
                    if(fail >= 5):
                        logger.info('Internet check fail, restart 4G modem...')
                        os.system('sudo service 4gpi-setup stop')
                        time.sleep(30)
                        os.system('sudo service 4gpi-setup start')
                        logger.warning('No connection to internet, exiting...')
                        fail = 0
                        break

                else:
                    debug = {}
                    try:
                        debug['cpu'] = psutil.cpu_percent()
                        debug['memory'] = psutil.virtual_memory().percent
                        debug['disk'] = psutil.disk_usage('/').percent
                        debug['timestamp'] = int(time.time())
                        debug['uptime'] = debug['timestamp'] - int(psutil.boot_time())
                    except:
                        logger.warning('Could not get system info')

                    try:
                        temp = psutil.sensors_temperatures()
                        debug['temp'] = temp['cpu_thermal'][0].current
                    except:
                        debug['temp'] = -1
                    
                    logger.info(json.dumps(debug))

                periodic_checking_timer = now + INTERNET_CHECK_INTERVAL
    
    except Exception as e:
        logger.warning(e)
        logger.info('Break received, exit...')

    os.kill(os.getpid(), signal.SIGINT)

class Streamer:
    def __init__(self):
        
        self.FRAME_READY_PIN = 25
        GPIO.setup(self.FRAME_READY_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(self.FRAME_READY_PIN, GPIO.FALLING, callback=self.frame_ready_callback, bouncetime=100)
        self.last_send = 0
        self.rate = 0
        self._devserial = '0000000000'
        self._mqttclient = MqttHandler()
        self.initialized = False
        self.bpm = None
        self.cropParams = 0
        self._id = 0
        self.mdata = 0
        self.mqtt_connected = False
        self.mqtt_connetion_in_progress = False
        self._capture = spi.CtsCapture(devId=0)
        self.mqttserver = ''
        self.mqttport = 8883
        self.mqttuser = ''
        self.mqttpassword = ''
        self.publishChannel = ''
        
        # used to handle SIGTERM, SIGINT
        self.sigHandler = SigtermHandler()

    def mqtt_connect(self,_devserial:str):
        self.mqtt_connetion_in_progress = True
        mqttConfig = {
            "host": self.mqttserver,
            "port": self.mqttport,
            "user": self.mqttuser,
            "passwd": self.mqttpassword,
            "clientId": f"writer_/device/{self._devserial}",
            "cleanSession": True,
            "keepAlive": 10,
            "usessl": True,
            "maxInflight": 1
        }


        self.qos = 0
        self._mqttclient.configure(**mqttConfig)

        # try to establish MQTT broker connection
        tries = 0
        while tries < 5:
            try:
                self._mqttclient.connect()
                break

            except (socket.gaierror, OSError) as e:
                logger.error(f'connection failed with error : {str(e)}, trying again in 5 seconds')
                time.sleep(5)
            finally:
                ###tries += 1
                tries = 0
        else:
            raise RuntimeError('client connection failed')

        tries = 0
        while tries < 5:
            if not self._mqttclient.isConnected:
                logger.info('waiting for client connection')
                time.sleep(2)
            else:
                logger.info('client connected')
                self.mqtt_connected = True
                break
        else:
            raise RuntimeError('client connection timed out')
        self.mqtt_connetion_in_progress = False

    def frame_ready_callback(self,channel):
        try:
            if self.bpm is None:
                ret, frame = self._capture.read()
                if not ret:
                    logger.warning('frame corrupted, skipping')
                else:
                    res, mdata = getmetadata(frame, framewidth, frameheight, 0)
                    logger.info(mdata)
                    logger.info('width = {}, height = {}'.format(framewidth, frameheight))
                    self.mdata = mdata
                    if not res:
                        logger.warning('metadata corrupted, skipping')
                    else:
                        ctsid = str(mdata["serial_number"])
                        ctsid = ctsid.zfill(4)
                        self._devserial = ctsid.zfill(10)
                        TDMS = parseConfigFile(ctsid, "/home/pi")
                        self.bpm = getBpm(TDMS)
                        SO = parseSoFile(ctsid, "/home/pi")
                        self.cropParams = np.array(SO)
                        logger.info("CTS serial = {}".format(self._devserial))
                        self.initialized = True
            
            elif self.mqtt_connected == True:
                ret, frame = self._capture.read()
                if not ret:
                    logger.warning('frame corrupted, skipping')
                else:
                    res, mdata = getmetadata(frame, framewidth, frameheight, 0, self.cropParams)
                    if not res:
                        logger.warning('metadata corrupted, skipping')
                    self.mdata = mdata

                if frame is not None:           
                    if hasattr(frame, "__len__"):
                        thermal_frame = frame[0:frameheight, 0:framewidth]
                        thermal_frame = thermal_frame.astype(int)
                        thermal_frame = np.abs(thermal_frame)
                        bson_packet = bson_compress(thermal_frame, self.mdata, self.bpm)
                        self._mqttclient.publish(self.publishChannel, bson_packet, qos=0)
                        print ("MQTT status=", mqtt.rc)
                        send_time = time.time()
                        self.rate = round(1/(send_time - self.last_send))
                        self.last_send = send_time
                        print('Send rate = {} frame/sec'.format(self.rate))
                        self._id +=1
                        if self._id % 2400 == 0:
                            logger.info('Frame sent: {}, rate: {} frame/sec'.format(self._id, self.rate))
            
            elif self.mqtt_connected  == False:
                if self.mqtt_connetion_in_progress == False:
                    regInfo = load_registration_info()
                    if regInfo is not None:
                        logger.info('Registration file found')
                        print(regInfo)
                    else:
                        logger.info('Registration file not found, attempt to register')
                        register_device(self._devserial)
                        regInfo = load_registration_info()
                    if regInfo is not None:
                        logger.info('Get device parameters from server...')
                        devInfo = get_device_info(self._devserial, regInfo)
                        if devInfo is not None:
                            self.mqttserver = devInfo['mqttHost']
                            self.mqttport = devInfo['mqttPort']
                            self.mqttuser = devInfo['mqttUser']
                            self.mqttpassword = devInfo['mqttPasswd']
                            self.publishChannel = devInfo['publishChannel'].replace('<serial_number>', self._devserial)
                            self.mqtt_connect(self._devserial)
                        else:
                            logger.error('Could not get MQTT info from server, exit now...')
                            os.kill(os.getpid(), signal.SIGINT)

            else:
                pass

        except Exception as e:
                logger.error('Error while uploading: ')
                logger.error(e)



if __name__=="__main__":
    try:
        x = threading.Thread(target=keeper_function, args=(1,))
        x.start()
        logger.info('Checking internet connection...')

        while(is_internet_on() == False):
            time.sleep(5.0)
        logger.info('Internet OK...')
        obj = Streamer()
        while 1:
            time.sleep(0.5)
    except KeyboardInterrupt:
        logger.info("Exiting program...")
        GPIO.cleanup()
        os.kill(os.getpid(), signal.SIGINT)
        

            
            
