from io import BytesIO
from nptdms import TdmsFile
import sys
import os
import numpy as np
import base64
import datetime
import time
import png
import spi
import bson

framewidth = 38
frameheight = 17
tempFactor = 256


#
# function for locating TDMS file, which contains important sensor properties and calibration information
#
def parseConfigFile(ctsid: str, configpath: str):
    """
    This function will:
    - list all the .tdms files in configpath
    - find the correct one using ctsid
    - read its content as numpy array via TdmsFile()
    - reshape the array and return it

    :param ctsid: used to find the correct config file
    :param configpath: where to search the config file
    :return: numpy array of config data
    """
    _retconfig = None
    _found = False
    _configfile = ''

    # re-format cts id string to match format used on filenames
    _ctsid = str(0) * (4 - len(ctsid)) + ctsid

    # search for tdms files
    for root, dirs, files in os.walk(configpath):
        if _found:
            break

        for file in files:
            if file.lower().endswith('.tdms'):
                try:
                    fname = file.split('.')[0]
                    idfield = fname.split('_')[0]

                    _found = _ctsid == idfield
                    if _found:
                        _configfile = os.path.join(root, file)
                        break

                except IndexError:
                    pass

    try:
        tdms_file = TdmsFile.read(_configfile)
        group = tdms_file['Untitled']
        channel = group['Untitled']

        # single row data
        channel_data = channel[:]

        # find out how many columns
        # assumption here is that we will have exactly 646 rows
        try:
            columns = int(len(channel_data) / 646)
            _retconfig = np.reshape(channel_data, (-1, columns))
            _retconfig = np.round(_retconfig, 10)

        except ValueError:
            raise ParseConfigError("malformed TDMS file")

    except FileNotFoundError:
        raise ParseConfigError("TDMS file not found for unit {0}".format(_ctsid))

    except ValueError:
        raise ParseConfigError("malformed TDMS file")

    return _retconfig

#
# function for locating and extracting image cropping parameters
#
def parseSoFile(ctsid: str, configpath: str):
    _found = False
    _sofile = ''

    # re-format cts id string to match format used on filenames
    _ctsid = str(0) * (4 - len(ctsid)) + ctsid

    # search for SO file: [ctsid]_oem.txt
    for root, dirs, files in os.walk(configpath):
        if _found:
            break

        for file in files:
            if file.lower().endswith('.txt'):
                try:
                    fname = file.split('.')[0]
                    idfield = fname.split('_')[0]

                    _found = _ctsid == idfield
                    if _found:
                        _sofile = os.path.join(root, file)
                        break

                except IndexError:
                    pass

    try:
        with open(_sofile, 'r') as f:
            lines = [line.rstrip('\n') for line in f]

            # second line will contain crop coordinates
            _nlist = lines[1].split()
            return int(float(_nlist[0])), int(float(_nlist[1])), int(float(_nlist[2])), int(float(_nlist[3]))

    except FileNotFoundError:
        raise ParseConfigError("SO config file not found for unit {0}".format(_ctsid))

    except (IndexError, ValueError):
        raise ParseConfigError("wrongly formatted SO config file for unit {0}".format(_ctsid))

#
# function for extracting a bad pixel mask from the TDMS file
#
def getBpm(tdms: np.ndarray = None):
    bpmask = None

    if tdms is not None:
        bpmask = np.asarray(tdms[:, 7].reshape((frameheight, framewidth)), dtype=np.bool)
        bpmask = np.logical_or(bpmask, np.asarray(tdms[:, 17].reshape(bpmask.shape), dtype=np.bool))
        bpmask = np.logical_or(bpmask, np.asarray(tdms[:, 16].reshape(bpmask.shape), dtype=np.bool))
        bpmask = bpmask.astype(np.uint8)

    return bpmask


def bson_compress(image: np.ndarray, metadata: dict, bp_map: np.ndarray = None):
    """

    Compression based on BSON package. Compresses image, metadata and bad pixel map (optional)
    Args:
        image: frame image as numpy array
        metadata: metadata, dict
        bp_map: bool nunmpy array

    Returns: Compredd messgae

    """

    # encode image
    bufferIM = BytesIO()
    w = png.Writer(image.shape[1], image.shape[0], bitdepth=16, alpha=False, greyscale=True)
    w.write(bufferIM, image)

    #construct message frame
    newFrame = {
        "dataValues": bufferIM.getvalue(),
        "metaData": metadata}

    # encode BPM - optional
    if bp_map is not None:
        bufferBPM = BytesIO()
        w = png.Writer(bp_map.shape[1], bp_map.shape[0], bitdepth=1, alpha=False, greyscale=True)
        w.write(bufferBPM, bp_map)

        newFrame["bpm"] = bufferBPM.getvalue()

    return bson.dumps(newFrame)


def LV_accl_conversion(val, bits=16):
    """
     This code replicate Labview accelerometer conversion (2's complement).
     Note that the conversion itself is not strictly 16bit 2's complement.
    """
    s = format(val, '0{0}b'.format(bits))

    # invert bits
    x = ["0" if b == '1' else "1" for b in s]

    x_ = "".join(x)

    out = int(x_[1:], 2) - int(x_[0]) * (2 ** (bits - 1)) + 1

    # Final conversion - deviation by 1296
    return out / 1296.

def getmetadata(rawframe, width: int, height: int, setupType: int, imageSize: np.ndarray = None):
    md = {}

    LensFOVs = ['35', '50', '65', '120', 'no lens']
    AssemblyVersion = ['CAL ASSEMBLY', 'OPTOPAX ASSEMBLY', 'Other']
    Materials = ['Aluminium', 'Plastic', 'Other']
    ProductVersion = ['RTS OEM', 'RTS PRO', 'NF', 'EVK', 'IOT', 'CTSH', 'CTSB']
    DieTypes = ['A', 'B', 'C']

    try:
        # get rows 17 and 18
        r17 = rawframe[17:18:].tolist()[0]
        r18 = rawframe[18:19:].tolist()[0]

        row = r17 + r18

        # capture timestamp
        now = datetime.datetime.now()
        capture_time = int(time.mktime(now.timetuple()) * 1e3 + now.microsecond / 1e3)
        md["timestamp"] = capture_time

        md["md_version"] = row[7] % 32

        # device type (setup type)
        md["device_type"] = setupType

        # serial number
        md["serial_number"] = int(swapWords(row[0:2]))

        # hw version
        md["hw_version"] = hex(swapWords(row[2:4]))[2:]

        reg_82_f = int(row[4] % 512)
        Mtrl_idx = int(reg_82_f // 64)
        md["Materials"] = Materials[Mtrl_idx] if Mtrl_idx < len(Materials) else ''

        # fov lens
        FOV_idx = int((reg_82_f % 64) % 8)
        md["fov"] = LensFOVs[FOV_idx] if FOV_idx < len(LensFOVs) else ''

        PrdtVer_idx = int((reg_82_f % 64) // 8)
        md["Product Version"] = ProductVersion[PrdtVer_idx] if PrdtVer_idx < len(ProductVersion) else ''
        AssyVer_idx = int(row[4] // 512)
        md["Assembly Version"] = AssemblyVersion[AssyVer_idx] if AssyVer_idx < len(AssemblyVersion) else ''

        # get x and y centre coordinates of the lens
        rawval = int(row[6])
        x_centre = float(int(rawval / 256) / 8)
        y_centre = float(int(rawval % 256) / 8)
        md["x_centre"] = x_centre
        md["y_centre"] = y_centre

        # get frame size info
        valid_cols = int(row[9] // 2048) + 23
        valid_rows = int(row[8] // 256) + 14
        lowByte = int(row[8] % 256)
        row_offset = int(lowByte // 8)
        col_offset = int(lowByte % 8)
        md["valid_cols"] = valid_cols
        md["valid_rows"] = valid_rows
        md["row_offset"] = row_offset
        md["col_offset"] = col_offset

        # FPGA temp
        md["fpga_temp"] = float(row[10] / 256)

        # Image sensor temp
        md["image_sensor"] = float(row[11] / 256)

        # led temp
        md["led_temp"] = float(row[12] / 256)

        # FT602
        md["ft602_temp"] = float(row[13] / 256)

        # flex1
        md["flex1_temp"] = float(row[14] / 256)

        # flex2
        md["flex2_temp"] = float(row[15] / 256)

        # uptime
        md["uptime"] = int(swapWords(row[56:58]))

        # frame counter
        md["frame_count"] = int(swapWords(row[58:60]))

        # X accel
        md["x_accel"] = LV_accl_conversion(row[60])

        # Y accel
        md["y_accel"] = LV_accl_conversion(row[61])

        # Z accel
        md["z_accel"] = LV_accl_conversion(row[62])

        # sw version
        md["sw_version"] = hex(swapWords(row[64:66]))[2:]

        # exposure
        md["exposure"] = int(swapWords(row[66:68]))

        # cts lifetime
        md["cts_lifetime"] = int(swapWords(row[70:72]))

        # frame witdh
        md["width"] = width

        # frame height
        md["height"] = height

        # temp factor
        md["temp_factor"] = tempFactor

        md["errors"] = 0

        if imageSize is not None:
            md["SDK_y0"] = int(imageSize[0])
            md["SDK_ys"] = int(imageSize[1])
            md["SDK_x0"] = int(imageSize[2])
            md["SDK_xs"] = int(imageSize[3])

        return True, md

    except IndexError:
        return False, None


def swapWords(words):
    if len(words) != 2:
        return 0
    return (words[1] << 16) + words[0]


def signedByte(byte):
    return (256 - byte) * (-1) if byte > 127 else byte


def signedByte12(val):
    return (4096 - val) * (-1) if val > 2047 else val


def compress2(image: np.ndarray, width: int, height: int):
    bufferIM = BytesIO()

    w = png.Writer(image.shape[1], image.shape[0], bitdepth=16, alpha=False, greyscale=True)
    w.write(bufferIM, image)

    bufferIM.flush()
    im = b'data:image/png;base64,' + base64.b64encode(bufferIM.getvalue())
    return im


def compress(image: np.ndarray, width: int, height: int):
    # convert frame data to string
    bitmapData = ''.join(str(image.tolist())).split()
    # Remove the square brackets and make it space delimited
    bitmapData = [s.replace('[', '').replace(']', '').replace(',', ' ') for s in bitmapData]

    # Join it into a long string
    bitmapData = ''.join(bitmapData)

    # create numpy array from bitmap string using uint16
    # im = np.fromstring(data, dtype=np.uint16, sep=' ').reshape(self.rows * self.cams, self.cols)
    im = np.fromstring(bitmapData, dtype=np.uint16, sep=' ').reshape(height, width)

    # create a png from the numpy array
    # for future reference:
    #  'L'
    #     greyscale(1 channel)
    #  'LA'
    #     greyscale with alpha (2 channel)
    #  'RGB'
    #     colour image(3 channel)
    #  'RGBA'
    #     colour image with alpha(4 channel)
    buffer = BytesIO()
    png.from_array(im, 'L').save(buffer)
    buffer.flush()

    result = base64.b64encode(buffer.getvalue())

    return result